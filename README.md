# NCRE

#### 介绍
这里主要是自己在学习NCRE考试相关科目时，自己整理的学习笔记、学习总结以及收集的学习资源，时间有限，目前还没能完整上传至仓库，后边我会不定时更新完的。
，
#### 适合人群
- 报考NCRE考试相关科目的成员
- 热爱学习cs知识的小伙伴
- 在校学习相关课程的大学生

#### 内容概要

0.  二级C语言
1.  三级数据库技术
2.  三级网络技术
3.  四级数据库原理
4.  四级网络原理
5.  其他科目部分学习资源

#### 使用说明

1.  内容为书籍主要知识点以及常考点，
2.  部分科目有题库知识点，
3.  阅读此系列文章可以帮助您快速、轻松考取相应证书！


#### 微信关注
     **青年之学** 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0721/172220_c7d6b04e_5725030.jpeg "Wechat.jpg")

#### 联系我
![输入图片说明](https://images.gitee.com/uploads/images/2021/0721/172354_6d2c8afd_5725030.jpeg "mywechat.jpg")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
